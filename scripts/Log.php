<?php

	// Zuständig für Logeinträge und Filehandling
	class Log {
		
		// Schreibt den übergebenen Text an das Ende der angegebene Datei
		public function writeLog($filename, $text) {
			
			$timestamp = time();
			$datum = date("d.m.Y",$timestamp);
			$uhrzeit = date("H:i",$timestamp);
			
			$handle = fopen($filename, "a+");
			
			fwrite($handle, $datum . "-" . $uhrzeit . "\t" . $text . "\r\n");
			
			fclose($handle);
		}
		
		// Liest die übergebene Datei zeilenweise ein und speichert diese
		// in einem Array
		public function getOptions($filename) {
			
			$arr = Array();
			$arr2 = Array();
			
			$file = file($filename);
			
			foreach ($file as $val) {
				
				$arr[] = $val;
			}
			
			$arr2[] = explode("=", $arr[0]);
			$arr2[] = explode("=", $arr[1]);
			
			$name = $arr2[0][1];
			$theme = $arr2[1][1];
			
			$arr = Array();
			
			array_push($arr, $name, $theme);
			
			return $arr;
		}
		
		public function setOptions($filename, $name, $theme) {
			
			$handle = fopen($filename, "w");
			
			fwrite($handle, "name=" . $name . "\r\n" . "theme=" . $theme);
			
			fclose($handle);
		}
	}

?>