<?php

	session_start();

	require_once 'Database.php';
	
	$db = new Database();
	$db -> connect();

	$mainthreadId = $_GET["m_id"];
?>
<!Doctype html>
<html>
	<head>
		<title>Forum</title>		<!-- Variable -->
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../css/style.css" />
	</head>
	<body>
		<header>
			<div id="h_name">
				Forenname			<!-- Variable -->
			</div>
			<div id="h_subname">
				Hauptthema			<!-- Variable -->
			</div>
		</header>
		<div id="login">
			<?php
			
				if(!isset($_SESSION["login"])) {
				
					echo '
					<form action="login.php" method="post">
						<label for="username">Benutzername</label>
						<input type="text" name="username" id="username"/>
						<label for="password">Kennwort</label>
						<input type="password" name="password" id="password" />
						<input type="submit" value="Login" name="login" id="loginBtn" />
						<a href="register.php">Registrieren</a>
					</form>';
					
				}
				else {

					echo '
					<div id="logout"><form action="login.php" method="post">
						<input type="submit" value="Logout" name="logout" />
					</form></div>';
				} 
			?> 
		</div>
		<div id="content">
			<div id="navi">
				<a href="../index.php">Übersicht</a> >
				<a href="#">Unterthemen</a>
			</div>
			<div id="newThread">
				<?php
				
					if(isset($_SESSION["login"])) {
						?>
						
						<form action="<?php echo "newThread.php?m_id=" . $mainthreadId; ?>" method="post" name="newSub">
							<input type="submit" value="Neues Thema" name="newThread"/><br /><br />
						</form>
						
						<?php
					}
				?>
			</div>
			<?php
			
				$arr = $db -> getSubThreads($mainthreadId);
				
				if(count($arr) != 0) {
					
					for ($i = 0; $i < count($arr); $i++) {
				
						echo "<div class='thema'>";
						echo "<a href='beitrag.php?m_id=" . $mainthreadId . "&s_id=" . $arr[$i] -> id . "'>" . $arr[$i] -> title . "</a>";
						echo "</div>";
					}
				}
				else {
					
					echo "Noch kein Thema vorhanden";
				}
				
			?>
		</div>
		<footer>
			<?php
			
				if(isset($_SESSION["auth"]) && $_SESSION["auth"] == "Administrator") {
					
					echo "<a href='scripts/privat.php'>Adminstration</a>";
				}
			
			?>
		</footer>
	</body>
</html>
<?php

	if(isset($_POST["saveSubThread"])) {
		
		$threadName = $_POST["threadName"];
		$threadText = $_POST["threadText"];
		
		$db -> createNewSubThread($threadName, $threadText, $mainthreadId, 2);
		// echo "<script>window.location.href = 'sub.php?m_id='" . $mainthreadId . "</script>";
	}

?>