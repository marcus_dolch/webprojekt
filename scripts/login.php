<?php
	 
	session_start();
	 
	require_once "Database.php"; 
	 
	$db = new Database();
	$db -> connect();
	

?>
<!Doctype html>
<html>
	<head>
		<title>Forum</title>		<!-- Variable -->
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../css/style.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	</head>
	<body>
		<header>
			<div id="h_name">
				Forenname			<!-- Variable -->
			</div>
			<div id="h_subname">
				Hauptthema			<!-- Variable -->
			</div>
		</header>
		<div id="login">

		</div>
		<div id="content">
			<div id="login_info">
				<?php
				
					if(isset($_POST["login"])) {
		
						$username = htmlentities($_POST["username"], ENT_QUOTES);
						$password = htmlentities(md5($_POST["password"]), ENT_QUOTES);
						
						$userdata = $db -> getUserLogin($username, $password);
		
						if($userdata["username"] == $username) {
		
							$_SESSION["username"] = $userdata["username"];
							$_SESSION["auth"] = $userdata["auth"];
							$_SESSION["login"] = TRUE;
							
							echo "Login erfolgreich! Sie werden weitergeleitet...";
							
							echo "<script>window.location.href = '../index.php'</script>";
						}
						else {
			
							echo "Der Benutzername oder das Kennwort stimmen nicht. Bitte erneut versuchen.";
						}
					}
					else if(isset($_POST["logout"])) {
		
						session_destroy();
						echo "<script>window.location.href = '../index.php'</script>";
					}
				
				?>
			</div>
		</div>
		<footer>
			
		</footer>
	</body>
</html>
