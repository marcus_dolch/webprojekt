<?php

	session_start();

	require_once 'Database.php';
	
	$db = new Database();
	$db -> connect();

	$mainthreadId = $_GET["m_id"];
	$subthreadId = $_GET["s_id"];

?>
<!Doctype html>
<html>
	<head>
		<title>Forum</title>		<!-- Variable -->
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../css/style.css" />
	</head>
	<body>
		<header>
			<div id="h_name">
				Forenname			<!-- Variable -->
			</div>
			<div id="h_subname">
				Hauptthema			<!-- Variable -->
			</div>
		</header>
		<div id="login">
			<?php
			
				if(!isset($_SESSION["login"])) {
				
					echo '
					<form action="login.php" method="post">
						<label for="username">Benutzername</label>
						<input type="text" name="username" id="username"/>
						<label for="password">Kennwort</label>
						<input type="password" name="password" id="password" />
						<input type="submit" value="Login" name="login" id="loginBtn" />
						<a href="register.php">Registrieren</a>
					</form>';
					
				}
				else {

					echo '
					<div id="logout"><form action="login.php" method="post">
						<input type="submit" value="Logout" name="logout" />
					</form></div>';
				} 
			?> 
		</div>
		<div id="content">
			<div id="navi">
				<a href="../index.php">Übersicht</a> >
				<?php
					echo "<a href='sub.php?m_id=" . $mainthreadId . "&s_id=" . $subthreadId . "'>Unterthemen</a>";
				?> >
				<a href="#">Aktueller Beitrag</a>
			</div>
			<div id="newThread">
				<?php
				
					if(isset($_SESSION["login"])) {
						?>
						
						<form action="<?php echo "newArticle.php?m_id=" . $mainthreadId . "&s_id=" . $subthreadId; ?>" method="post">
							<input type="submit" value="Antworten" /><br /><br />
						</form>
						
						<?php
					}
				?>
			</div>
			<?php
				
				$arr = $db -> getArticle($subthreadId);
				
				if(count($arr) != 0) {
					
					for($i = 0; $i < count($arr); $i++) {
						
						echo "<div class='article'>";
						echo "<div class='article_left'>";
						echo "<p class='a_user'>" . $db -> getUserById($arr[$i] -> userId) ."</p>";
						echo "<p class='a_date'>" . $arr[$i] -> date . "</p>";
						echo "</div>";
						echo "<div class='article_right'>";
						echo "<p class='a_title'>" . $arr[$i] -> title . "</p>";
						echo "<p class='a_text'>" . $arr[$i] -> text . "</p>";
						echo "</div>";
						echo "</div>";
					}
				}
				else {
				
					echo "Kein Beitrag vorhanden...";
				}
				
			?>
		</div>
		<footer>
			
		</footer>
	</body>
</html>