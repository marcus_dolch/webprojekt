<?php

	/*
	 * Author:		Marcus Dolch
	 * Datum:		14.06.2013
	 * Version:		1.0
	 * Funktion:	Datenbank Transaktionen 
	 */

	// Einbinden der benötigten Klassen
	require_once 'MainThread.php';
	require_once 'SubThread.php';
	require_once 'Article.php';
	require_once 'User.php';

	class Database {
		
		// Variablen
		private $db;
		private $timestamp;
		private $date;
		
		// Gibt das aktuelle Datum im Format JJJJ-MM-TT zurück
		public function getDateNow() {
			
			$this -> timestamp = time();
			$date = date("Y-m-d", $this -> timestamp);
			
			return $date;
		}
	
		// Stellt eine Verbindung zur Datenbank her
		// Wird bis jetzt beim Start der Seite aufgerufen, sollte aber
		// zum Schluss durch jede Funktion einzeln aufgerufen und wieder
		// geschlossen werden.
		public function connect() {
	
			// mysql Passwort muss noch vergeben werden, wenn Datenbank im Netz läuft
			$this -> db = new mysqli("localhost", "root", "", "forum");
	
			if ($this -> db -> connect_errno) {
	
				$err = "Verbindung fehlgeschlagen: (" . $this -> db -> connect_errno . ") " 
						. $this -> db -> connect_error;
			} 
		}
		
		// Schließt die Datenbankverbindung
		public function disconnect() {
			
			$this -> db -> close();
		}
		
		// Erzeugt einen neuen Mainthread, kann nur vom Privaten Bereich aufgerufen werden
		public function createNewMainThread($mainthread) {
			
			$date = $this->getDateNow();
			
			// Wichtig da Konstanten nicht referenziert werden können(bind_param)
			$user = 2;	
			
			if(mysqli_connect_errno() == 0) {
				
				$query = "INSERT INTO mainthread(m_title, m_date, m_u_id) VALUES(?, ?, ?)";
				$entry = $this -> db -> prepare($query);
				$entry -> bind_param('ssi', $mainthread, $date, $user);
				$entry -> execute();
				
				if($entry -> affected_rows == 1) {
					
					return true;	
				}
			}
			else {
				
				return false;
			}
		}
		
		// Liest alle Mainthreads aus und gibt diese als Array zurück
		public function getMainThreads() {

			$arr = Array();
	
			$query = 'SELECT * FROM mainthread ORDER BY m_id';
			$result = $this -> db -> query($query);
		
			while ($row = $result -> fetch_object()) {
	
				$mt = new MainThread();
				$mt -> setId($row -> m_id);
				$mt -> setTitle($row -> m_title);
				$mt -> setUserId($row -> m_u_id);
				$mt -> setDate($row -> m_date);
				
				$arr[] = $mt;
			}
			return $arr;
		}
		
		// Löscht den Mainthread mit der übergebenen ID
		public function deleteMainThread($mainthread) {
		
			$query = "DELETE FROM mainthread WHERE m_title='" . $mainthread . "' LIMIT 1";
			$result = $this -> db -> prepare($query);
			$result -> execute();
			$result -> close();
		}
		
		// Ändert den Titel des Mainthreads mit der übergebenen ID
		public function updateMainThread($mainthread, $update) {
		
			$query = "UPDATE mainthread SET m_title='{$update}' WHERE m_title='{$mainthread}' LIMIT 1";
			$result = $this -> db -> prepare($query);
			$result -> execute();
			$result -> close();
		}
		
		// Liest alle Subthreads mit der übergebenen ID aus und gibt diese als Array zurück
		public function getSubThreads($mainthreadId) {

			$arr = Array();
	
			$query = "SELECT * FROM subthread WHERE s_m_id='{$mainthreadId}' ORDER BY s_id";
			$result = $this -> db -> query($query);
		
			while ($row = $result -> fetch_object()) {
	
				$st = new SubThread();
				$st -> setId($row -> s_id);
				$st -> setTitle($row -> s_title);
				$st -> setSubThreadId($row -> s_m_id);
				$st -> setDate($row -> s_date);
				
				$arr[] = $st;
			}
			return $arr;
		}
		
		// Löscht den Subthread mit der übergebenen ID
		public function deleteSubThread($subthread) {
		
			$query = "DELETE FROM subthread WHERE s_title='{$subthread}' LIMIT 1";
			$result = $this -> db -> prepare($query);
			$result -> execute();
			$result -> close();
		}
		
		// Ändert den Titel des Subthreads mit der übergebenen ID
		public function updateSubThread($subthread, $update) {
		
			$query = "UPDATE subthread SET s_title='{$update}' WHERE s_title='{$subthread}' LIMIT 1";
			$result = $this -> db -> prepare($query);
			$result -> execute();
			$result -> close();
		}
		
		// Erstellt einen neuen Subthread mit den übergebenen Werten
		public function createNewSubThread($subthreadName, $subthreadText, $mainthreadId, $userId) {
			
			$date = $this -> getDateNow();
			$subthreadId = $this -> getSubThreadId();
			var_dump($subthreadId);
			$subthreadId++;
			
			$query = "INSERT INTO subthread(s_id, s_title, s_date, s_m_id) 
					  VALUES({$subthreadId}, '{$subthreadName}', NOW(), {$mainthreadId})";						  					  		
			$result = $this -> db -> query($query);
			
			$query = "INSERT INTO article(a_title, a_text, a_date, a_s_id, a_u_id) 
					  VALUES('{$subthreadName}', '{$subthreadText}', NOW(), {$subthreadId}, {$userId})";					  					  	
			$result = $this -> db -> query($query);					  	  				  
		}
		
		// Liest die aktuell höchste ID aus und gibt diese zurück
		// Wird in der Funktion createNewSubThread verwendet
		public function getSubThreadId() {
			
			$query = "SELECT MAX(s_id) FROM subthread";
			
			$result = $this -> db -> prepare($query);
			$result -> execute();
			
			$result -> bind_result($id);
			
			while($result -> fetch()) {
			
				return $id;
			}
		}
		
		// Liest die Beiträge aus mit der übergebenen ID
		public function getArticle($subthreadId) {
			
			$arr = Array();
			
			$query = "SELECT * FROM article WHERE a_s_id={$subthreadId} ORDER BY a_date, a_id";
			
			$result = $this -> db -> query($query);
			
			while($row = $result -> fetch_object()) {
				
				$article = new Article();
				$article -> setId($row -> a_id);
				$article -> setTitle($row -> a_title);
				$article -> setSubthreadId($row -> a_s_id);
				$article -> setDate($row -> a_date);
				$article -> setText($row -> a_text);
				$article -> setUserId($row -> a_u_id);
				
				$arr[] = $article;
			}
			
			return $arr;
		}
		
		// Speichert einen neuen Beitrag
		public function createArticle($title, $text, $subthreadId, $userId) {
			
			$query = "INSERT INTO article(a_title, a_text, a_date, a_s_id, a_u_id) 
					  VALUES('{$title}', '{$text}', NOW(), {$subthreadId}, {$userId})";
			$result = $this -> db -> query($query);
		}
		
		// Liest alle Benutzer aus und gibt diese als Array zurück
		public function getUser($QueryUsername = "", $QueryUserId = "") {
			
			$arr = Array();
			
			if($QueryUsername == "" && $QueryUserId == "") {
			
				$query = "SELECT * FROM user";
			}
			else {
				
				$query = "SELECT * FROM user WHERE u_name='{$QueryUsername}'";
			}
			
			$result = $this -> db -> query($query);
		
			while ($row = $result -> fetch_object()) {
	
				$user = new User();
				$user -> setId($row -> u_id);
				$user -> setName($row -> u_name);
				$user -> setEmail($row -> u_email);
				$user -> setPassword($row -> u_password);
				$user -> setAuthority($row -> u_authority);
				
				$arr[] = $user;
			}
			return $arr;
		}
		
		// Liefert den Benutzernamen der übergebenen Id zurück
		public function getUserById($userId) {
			
			$query = "SELECT u_name FROM user WHERE u_id={$userId}";
			
			$result = $this -> db -> prepare($query);
			$result -> execute();
			
			$result -> bind_result($username);
			
			while($result -> fetch()) {
			
				return $username;
			}
		}
		
		// Löscht den User mit der übergebenen ID
		public function deleteUser($user) {
		
			$query = "DELETE FROM user WHERE u_name='{$user}' LIMIT 1";
			$result = $this -> db -> prepare($query);
			$result -> execute();
			$result -> close();
		}
		
		// Prüft ob der Benutzer in der Datenbank existiert
		public function getUserLogin($username, $password) {
						
			$arr = Array();
				
			$query = "SELECT u_name, u_password, u_authority FROM user WHERE u_name='{$username}' AND u_password='{$password}'";
			
			$result = $this -> db -> prepare($query);
			$result -> execute();
			
			$result -> bind_result($un, $pw, $auth);
			
			while($result -> fetch()) {
					
				$arr["username"] = $un;
				$arr["password"] = $pw;
				$arr["auth"] = $auth;
			
				return $arr;
			}
		}
		
		// Trägt einen neuen Benutzer in die Datenbank ein
		public function registerUser($name, $email, $password) {
			
			$tmp = md5($password);
				
			$authority = "user";				
				
			$query = "INSERT INTO user(u_name, u_email, u_password, u_authority) 
					  VALUES(?, ?, ?, ?)";	
			$entry = $this -> db -> prepare($query);
			$entry -> bind_param('ssss', $name, $email, $tmp, $authority);
			$entry -> execute();
		}

		// Noch nicht implementiert
		// Prüft ob ein Benutzername bereits in der Datenbank gespeichert ist.
		public function getUsername($username) {
			
			$query = "SELECT u_id FROM user WHERE u_name='{$username}'";
			
			$result = $this -> db -> prepare($query);
			$result -> execute();
			$result -> bind_result($uid);
			
			$user = -1;
			
			while($result -> fetch()) {
					
				$user = $uid;	
			}
			
			echo $user;
			
			if($user == -1) {
				
				return false;
			}
			else {
				
				return true;
			}
			
		}
	}

?>























