<?php

	class SubThread {
		
		public $id = -1;
		public $title = "Default";
		public $subThreadId = -1;
		public $date = "";
		
		// Getter
		function getId() {
			
			return $this->id;
		}
		
		function getTitle() {
			
			return $this->title;
		}
		
		function getSubthreadId() {
			
			 return $this->subThreadId;
		}
		
		function getDate() {
				
			return $this->date;
		}
		
		// Setter
		function setId($id) {
			
			$this->id = $id;
		}
		
		function setTitle($title) {
			
			$this->title = $title;
		}
		
		function setSubthreadId($subthreadId) {
			
			$this->subThreadId = $subthreadId;
		}
		
		function setDate($date) {
			
			$this->date = $date;
		}	
	}

?>