<?php

	session_start();

	$mainthreadId = $_GET["m_id"];
	
?>
<!Doctype html>
<html>
	<head>
		<title>Forum</title>		<!-- Variable -->
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../css/style.css" />
	</head>
	<body>
		<header>
			<div id="h_name">
				Forenname			<!-- Variable -->
			</div>
			<div id="login">

			</div>
			<div id="h_subname">
				Hauptthema			<!-- Variable -->
			</div>
		</header>
		<div id="content">
			<form action="<?php echo "sub.php?m_id=" . $mainthreadId; ?>" method="post" id="newThread">
				<p>
					Neues Thema erstellen
				</p><br />
				<input type="text" placeholder="Titel" name="threadName"/><br />
				<textarea name="threadText">
					
				</textarea><br />
				<input type="submit" value="Senden" name="saveSubThread" />
				<input type="reset" value="Leeren" />
			</form>
		</div>
		<footer>
			<a href="../index.php">Zurück</a>
		</footer>
	</body>
</html>