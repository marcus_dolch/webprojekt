<?php

	/*
	 * Author:		Marcus Dolch
	 * Datum:		14.06.2013
	 * Version:		1.0
	 * Funktion:	User Entität
	 */

	class User {
		
		private $id = -1;
		private $name = "";
		private $email = "";
		private $password = "";
		private $authority = "";
		
		
		// Getter
		public function getId() {
			
			return $this -> id;
		} 
		
		public function getName() {
			
			return $this -> name;
		}
		
		public function getEmail() {
			
			return $this -> email;
		}
		
		public function getPassword() {
			
			return $this -> password;
		}
		
		public function getAuthority() {
			
			return $this -> authority;
		}
		
		// Setter
		public function setId($id) {
			
			$this -> id = $id;
		} 
		
		public function setName($name) {
			
			$this -> name = $name;
		}
		
		public function setEmail($email) {
			
			$this -> email = $email;
		}
		
		public function setPassword($password) {
			
			$this -> password = $password;
		}
		
		public function setAuthority($authority) {
			
			$this -> authority = $authority;
		}
	}

?>