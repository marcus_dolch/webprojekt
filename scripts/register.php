<?php
	 
	session_start();
	 
	require_once 'Database.php';
	require_once 'Log.php';
	
	$db = new Database();
	$db -> connect();
	
	$l = new Log();
	$val = $l->getOptions("../options.conf");

	$_SESSION["forenname"] = $val[0];
	$_SESSION["hauptthema"] = $val[1];
	
?>
<!Doctype html>
<html>
	<head>
		<title><?php echo $_SESSION["forenname"]; ?></title>
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../css/style.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	</head>
	<body>
		<header>
			<div id="h_name">
				<?php echo $_SESSION["forenname"]; ?>
			</div>
			<div id="h_subname">
				<?php echo $_SESSION["hauptthema"]; ?>
			</div>
		</header>
		<div id="login">
			<?php
			
				if(!isset($_SESSION["login"])) {
				
					echo '
					<form action="login.php" method="post">
						<label for="username">Benutzername</label>
						<input type="text" name="username" id="username"/>
						<label for="password">Kennwort</label>
						<input type="password" name="password" id="password" />
						<input type="submit" value="Login" name="login" id="loginBtn" />
						<a href="register.php">Registrieren</a>
					</form>';
					
				}
				else {

					echo '
					<div id="logout"><form action="login.php" method="post">
						<input type="submit" value="Logout" name="logout" />
					</form></div>';
				} 
			?> 
		</div>
		<div id="content">
			<form action="../index.php" method="post" id="register">
				<label for="name">Benutzername:</label><br />
				<input type="text" name="name" required/><br />
				<label for="email">E-Mail:</label><br />
				<input type="email" name="email" required/><br />
				<label for="pass1">Passwort:</label><br />
				<input type="password" name="pass1" required/><br />
				<label for="pass2">Passwort wiederholen:</label><br />
				<input type="password" name="pass2" required/><br />
				<input type="submit" name="register" value="Anmelden"/>
                <a href="../index.php">Abbrechen</a>
            </form>

		</div>
		<footer>
			
		</footer>
	</body>
</html>