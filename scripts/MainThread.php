<?php

	class MainThread {
		
		public $id = -1;
		public $title = "Default";
		public $userId = -1;
		public $date = "";
		
		// Getter
		function getId() {
			
			return $this->id;
		}
		
		function getTitle() {
			
			return $this->title;
		}
		
		function getUserId($id) {
			
			 return $this->userId;
		}
		
		function getDate() {
				
			return $this->date;
		}
		
		// Setter
		function setId($id) {
			
			$this->id = $id;
		}
		
		function setTitle($title) {
			
			$this->title = $title;
		}
		
		function setUserId($userId) {
			
			$this->userId = $userId;
		}
		
		function setDate($date) {
			
			$this->date = $date;
		}
		
		function toString() {
			
			return $this->id + ": " + $this->title + " Ersteller: " + $this->userId . " am: " . $this->date;
		}
		
	}

?>