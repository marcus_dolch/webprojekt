<?php

	session_start();
	$_SESSION["login"] = true;

	require_once 'Database.php';
	require_once 'User.php';
	require_once 'Log.php';
	
	$db = new Database();
	$db -> connect();

	$subthreadId = $_GET["s_id"];
	$mainthreadId = $_GET["m_id"];
	
?>
<!Doctype html>
<html>
	<head>
		<title>Forum</title>		<!-- Variable -->
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../css/style.css" />
	</head>
	<body>
		<header>
			<div id="h_name">
				Forenname			<!-- Variable -->
			</div>
			<div id="h_subname">
				Hauptthema			<!-- Variable -->
			</div>
		</header>
		<div id="login">
			<?php
			
				if($_SESSION["login"] == FALSE) {
				
					echo '
					<form action="login.php" method="post">
						<label for="username">Benutzername</label>
						<input type="text" name="username" />
						<label for="password">Kennwort</label>
						<input type="password" name="password" />
						<input type="submit" value="Login" />
					</form>';
				}
			?> 
		</div>
		<div id="content">
			<form action="" method="post" id="newThread" name="newThread">
				<p>
					Antwort erstellen
				</p><br />
				<input type="text" placeholder="Titel" name="articleName"/><br />
				<textarea name="articleText">
					
				</textarea><br />
				<input type="submit" value="Senden" name="saveArticle" />
				<input type="reset" value="Leeren" />
			</form>
		</div>
		<footer>
			<a href="../index.php">Zurück</a>
		</footer>
	</body>
</html>
<?php

	if(isset($_POST["saveArticle"])) {
		
		$user = new User();
		
		$arr = $db -> getUser($_SESSION["username"]);
		
		$db -> createArticle($_POST["articleName"], $_POST["articleText"], $subthreadId, $arr[0]->getId());
		
		echo "<script>window.location.href = 'beitrag.php?m_id=" . $mainthreadId . "&s_id=" . $subthreadId . "'</script>";
	}

?>