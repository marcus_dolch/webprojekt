<?php

	class Article {
		
		public $id = -1;
		public $title = "Default";
		public $subthreadId = -1;
		public $userId = -1;
		public $date = "";
		public $text = "";
		
		// Getter
		function getId() {
			
			return $this->id;
		}
		
		function getTitle() {
			
			return $this->title;
		}
		
		function getSubthreadId() {
			
			 return $this->subthreadId;
		}
		
		function getDate() {
				
			return $this->date;
		}
		
		function getText() {
			
			return $this->text;
		}
		
		function getUserId() {
			
			return $this->userId;
		}
		
		// Setter
		function setId($id) {
			
			$this->id = $id;
		}
		
		function setTitle($title) {
			
			$this->title = $title;
		}
		
		function setSubthreadId($subthreadId) {
			
			$this->subthreadId = $subthreadId;
		}
		
		function setDate($date) {
			
			$this->date = $date;
		}
		
		function setText($text) {
			
			$this->text = $text;
		}
		
		function setUserId($userId) {
			
			$this->userId = $userId;
		}
	}

?>