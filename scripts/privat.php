<?php

	session_start();

	function __autoload($classname) {
		
		$path = $classname . '.php';
		
		if(file_exists($path)) {
			
			require_once $path;
		}
	}

	$db = new Database();
	$db -> connect();
	
	$log = new Log();

?>
<!Doctype html>
<html>
	<head>
		<title>Administration</title>
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href=".././css/style.css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script>
			$(function() {
				$("#content").tabs();
			});
		</script>
	</head>
	<body>
		<header>
			<div id="h_name">
				<?php echo $_SESSION["forenname"]; ?>
			</div>
			<div id="h_subname">
				<?php echo $_SESSION["hauptthema"]; ?>
			</div>
		</header>
		<div id="login">
			
		</div>
		<div id="content">
			<ul>
				<li>
					<a href="#tabs-1">Allgemeine Einstellungen</a>
				</li>
				<li>
					<a href="#tabs-2">Benutzerverwaltung</a>
				</li>
				<li>
					<a href="#tabs-3">Themenverwaltung</a>
				</li>
			</ul>
			<div id="tabs-1">
				<form action="" method="POST">
					<label for="titel" class="p_label">Titel:</label>
					<input type="text" name="titel" value="<?php echo $_SESSION["forenname"]; ?>"/>
					<br />
					<label for="thema"class="p_label">Thema:</label>
					<input type="text" name="thema" value="<?php echo $_SESSION['hauptthema']; ?>"/>
					<br />
					<br />
					<input type="submit" name="submit_allgemein" value="Speichern" />
				</form>
			</div>
			<div id="tabs-2">
				<form action="privat.php" method="post">
					<label for="username">Benutzer</label>
					<br />
					<select name="username" size="5">
						<?php
						
							$arrUser = $db -> getUser();
						
							for ($i=0; $i < count($arrUser); $i++) {
								
								echo "<option>" . $arrUser[$i] -> getName() . "</option>"; 
							}
						?>
					</select>
					<br />
					<input type="submit" value="Löschen" name="deleteUser" />
				</form>
			</div>
			<div id="tabs-3">
				<form action="privat.php" method="post">
					<label for="mainthreads">Hauptthemen</label>
					<br />
					<select name="mainthread" size="5">
						<?php
							$arr = $db -> getMainThreads();
							
							for ($i=0; $i < count($arr); $i++) {
									
								echo "<option>" . $arr[$i] ->title . "</option>"; 
							}
						?>
					</select>
					<br />
					<input type="text" name="txtRenameMainthread" />
					<input type="submit" value="Neu Anlegen" name="createMainthread"/>
					<input type="submit" value="Umbenennen" name="updateMainthread"/>
					<input type="submit" value="Löschen" name="deleteMainthread"/>
				</form>
				<hr />
				<form action="privat.php" method="post">
					<label for="subthreads">Unterthemen</label>
					<br />
					<select name="subthreads" size="5">
						<?php
							$arr = $db -> getSubThreads();
							
							for ($i=0; $i < count($arr); $i++) {
								
								echo "<option>" . $arr[$i] ->title . "</option>"; 
							}
						?>
					</select>
					<br />
					<input type="text" name="txtRenameSubthread" />
					<input type="submit" value="Umbenennen" name="updateSubthread"/>
					<input type="submit" value="Löschen" name="deleteSubthread"/>
				</form>
				<hr />
			</div>
		</div>
		<footer>
			<a href=".././index.php">Exit</a>
		</footer>
	</body>
</html>
<?php

	if(isset($_POST["createMainthread"])) {
		
		$name = $_POST["txtRenameMainthread"];
		$db -> createNewMainThread($name);
		
		echo "<script>window.location.href = 'privat.php'</script>";
		// echo "<script>window.location.href = 'privat.php#tabs-3'</script>";
	}
	else if(isset($_POST["deleteMainthread"])) {
			
		$name = $_POST["mainthreads"];
		$db -> deleteMainThread($name);
		
		echo "<script>window.location.href = 'privat.php'</script>";
	}
	else if(isset($_POST["updateMainthread"])) {
		
		$name = $_POST["mainthreads"];
		$newName = $_POST["txtRenameMainthread"];
		$db -> updateMainThread($name, $newName);
		
		echo "<script>window.location.href = 'privat.php'</script>";
	}
	else if(isset($_POST["deleteSubthread"])) {
		
		$name = $_POST["subthreads"];
		$db -> deleteSubThread($name);
		
		echo "<script>window.location.href = 'privat.php'</script>";
	}
	else if(isset($_POST["updateSubthread"])) {
		
		$name = $_POST["subthreads"];
		$newName = $_POST["txtRenameSubthread"];
		$db -> updateSubThread($name, $newName);
		
		echo "<script>window.location.href = 'privat.php'</script>";
	}
	else if(isset($_POST["deleteUser"])) {
		
		$name = $_POST["username"];
		$db -> deleteUser($name);
		
		echo "<script>window.location.href = 'privat.php'</script>";
	}
	else if(isset($_POST["submit_allgemein"])) {
	
		$forenname = $_POST["titel"];
		$forenthema = $_POST["thema"];
		
		$log -> setOptions("../options.conf", $forenname, $forenthema);
		echo "<script>window.location.href = 'privat.php'</script>";
	}

?>