<?php

	/*
	 * Author:		Marcus Dolch
	 * Datum:		14.06.2013
	 * Version:		1.0
	 * Funktion:	Startseite des Forums
	 */
	 
	session_start();

    function __autoload($classname) {

        $pfad = 'scripts/' . $classname . '.php';

        if (file_exists($pfad)) {

            require_once $pfad;
        }
        else {

            return false;
        }
    }


	
	$db = new Database();
	$db -> connect();
	
	$l = new Log();
	$val = $l->getOptions("options.conf");

	$_SESSION["forenname"] = $val[0];
	$_SESSION["hauptthema"] = $val[1];
	
?>
<!Doctype html>
<html>
	<head>
		<title><?php echo $_SESSION["forenname"]; ?></title>
		<meta name="description" content="Forum">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript">
		<meta name="author" content="Marcus Dolch">
		<meta charset="UTF-8">
		<link rel="stylesheet" href="css/style.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	</head>
	<body>
		<header>
			<div id="h_name">
				<?php echo $_SESSION["forenname"]; ?>
			</div>
			<div id="h_subname">
				<?php echo $_SESSION["hauptthema"]; ?>
			</div>
		</header>
		<div id="login">
			<?php
			
				if(!isset($_SESSION["login"])) {
				
					echo '
					<form action="scripts/login.php" method="post">
						<label for="username">Benutzername</label>
						<input type="text" name="username" id="username"/>
						<label for="password">Kennwort</label>
						<input type="password" name="password" id="password" />
						<input type="submit" value="Login" name="login" id="loginBtn" />
						<a href="scripts/register.php">Registrieren</a>
					</form>';
					
				}
				else {

					echo '
					<div id="logout"><form action="scripts/login.php" method="post">
						<input type="submit" value="Logout" name="logout" />
					</form></div>';
				} 
			?> 
		</div>
		<div id="content">
			<?php
				
				$arr = $db -> getMainThreads();
				
				for ($i = 0; $i < count($arr); $i++) {
				
					echo "<div class='thema'>";
					echo "<a href='scripts/sub.php?m_id=" . $arr[$i] -> id . "'>" . $arr[$i] -> title . "</a>";
					echo "</div>";
				}
				
			?>
		</div>
		<footer>
			<?php
			
				if(isset($_SESSION["auth"]) && $_SESSION["auth"] == "Administrator") {
					
					echo "<a href='scripts/privat.php'>Adminstration</a>";
				}
			
			?>
		</footer>
	</body>
</html>
<?php

	if(isset($_POST["register"])) {
		
		$name = $_POST["name"];
		$email = $_POST["email"];
		$password = $_POST["pass2"];
		
		$db -> registerUser($name, $email, $password);
	}
    else if(isset($_POST["reg_cancel"])) {

        echo "<script>window.location.href = '../index.php'</script>";
    }

?>